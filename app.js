// Visualizer
const arrayLength = 8;
const maxValue = 100;
let randomArray = [];
let visualizer = document.getElementById('list');
const create = document.getElementById("create");
const play = document.getElementById("play");

function swap(el1, el2) {
    let temp = el1.innerText;
    el1.innerText = el2.innerText;
    el2.innerText = temp;
}

// Disables newArray buttons used in conjunction with enable, so that we can disable during sorting and enable buttons after it
function disableNewArrayBtn() {
    create.disabled = true;
}

// Enables newArray buttons used in conjunction with disable
function enableNewArrayBtn() {
    create.disabled = false;
}

// Used in async function so that we can so animations of sorting, takes input time in ms (1000 = 1s)
function waitforme(milisec) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('')
        }, milisec);
    })
}

let delay = 2560;

function createRandomArray() {
    deleteChild();
    randomArray = [];
    for (let i = 0; i < arrayLength; i++) {
        randomArray.push(Math.floor(Math.random() * 100) + 1);
    }

    for (let i = 0; i < arrayLength; i++) {
        randomArray.push(Math.floor(Math.random() * 100) + 1);

        let item = document.createElement("div");
        item.setAttribute('class', 'item');
        let bubble = document.createElement("div");
        bubble.setAttribute('class', 'bubble');
        bubble.innerText = randomArray[i];
        item.appendChild(bubble);
        visualizer.appendChild(item);
    }
}

function deleteChild() {
    visualizer.innerHTML = '';
}


create.addEventListener('click', () => {
    // enableSortingBtn();
    createRandomArray();
})


async function bubble() {
    const ele = document.querySelectorAll(".bubble");

    for (let i = 0; i < ele.length - 1; i++) {
        for (let j = 0; j < ele.length - i - 1; j++) {
            ele[j].style.backgroundColor = '#182825';
            ele[j + 1].style.backgroundColor = '#182825';
            if (parseInt(ele[j].innerText) > parseInt(ele[j + 1].innerText)) {
                await waitforme(delay);
                swap(ele[j], ele[j + 1]);
            }
            ele[j].style.background = '#AFA98D';
            ele[j + 1].style.background = '#AFA98D';
        }
        ele[ele.length - 1 - i].style.background = '#22AED1';

    }
    ele[0].style.background = '#22AED1';

}


play.addEventListener('click', async function () {
    disableNewArrayBtn();
    await bubble();
    enableNewArrayBtn();
});

function setCurrent(now, next) {
    for (let element of elements) {
        element.style.backgroundColor = ''
    }

    gsap.set([elements[now], elements[next]], {
        backgroundColor: 'rgb(52, 196, 91)'
    })
}
// Visualizer


// Blobs and Bubbles Animations 

const tween = KUTE.fromTo('#blob0', {
    path: '#blob0'
}, {
    path: '#blob1'
}, {
    repeat: 1000,
    duration: 6000,
    yoyo: true
});
tween.start();

const tween1 = KUTE.fromTo('#blob2', {
    path: '#blob2'
}, {
    path: '#blob3'
}, {
    repeat: 1000,
    duration: 6000,
    yoyo: true
});
tween1.start();

const tween2 = KUTE.fromTo('#blob4', {
    path: '#blob4'
}, {
    path: '#blob5'
}, {
    repeat: 1000,
    duration: 6000,
    yoyo: true
});
tween2.start();

const tween3 = KUTE.fromTo('#blob6', {
    path: '#blob6'
}, {
    path: '#blob7'
}, {
    repeat: 1000,
    duration: 6000,
    yoyo: true
});
tween3.start();

const tween4 = KUTE.fromTo('#blob8', {
    path: '#blob8'
}, {
    path: '#blob9'
}, {
    repeat: 1000,
    duration: 6000,
    yoyo: true
});
tween4.start();


gsap.to("#bubble1", {
    duration: 12,
    ease: "none",
    y: "-=650", //move each box 500px to right
    modifiers: {
        x: gsap.utils.unitize(y => parseFloat(y) % 650) //force x value to be between 0 and 500 using modulus
    },
    repeat: -1
});

gsap.to("#bubble2", {
    duration: 10,
    ease: "none",
    y: "-=650", //move each box 500px to right
    modifiers: {
        x: gsap.utils.unitize(y => parseFloat(y) % 650) //force x value to be between 0 and 500 using modulus
    },
    repeat: -1
});

gsap.to("#bubble3", {
    duration: 8,
    ease: "none",
    y: "-=650", //move each box 500px to right
    modifiers: {
        x: gsap.utils.unitize(y => parseFloat(y) % 650) //force x value to be between 0 and 500 using modulus
    },
    repeat: -1
});

gsap.to("#bubble4", {
    duration: 6,
    ease: "none",
    y: "-=650", //move each box 500px to right
    modifiers: {
        x: gsap.utils.unitize(y => parseFloat(y) % 650) //force x value to be between 0 and 500 using modulus
    },
    repeat: -1
});

Draggable.create('#visual');


let dragStop = document.getElementById("dragBounds");
let snapStops = [0, 100, 200, 300];

Draggable.create("#dragControl", {
    type: "x",
    throwProps: true,
    bounds: {
        minX: 700,
        maxX: -700
    },
    edgeResistance: 1,
    snap: snapStops,
    onDrag: makeEmMove,
    onThrowUpdate: makeEmMove
});

function makeEmMove() {
    TweenMax.set(".panelWrap", {
        x: this.x * 6
    });
}


let btn = document.getElementById("btn");

btn.addEventListener('click', () => {
    TweenMax.fromTo("#visual", 10, {
        scale: 1
    }, {
        scale: 3,
        ease: Linear.easeNone
    })
})

// gsap.registerPlugin(ScrollTrigger);

// let arrow = document.querySelector('.arrow');
// let arrowRight = document.querySelector('.arrow-right');

// if (arrow) {
//     gsap.to(arrow, {
//         y: 12,
//         ease: "power1.inOut",
//         repeat: -1,
//         yoyo: true
//     });
// }

// if (arrowRight) {
//     gsap.to(arrowRight, {
//         x: -12,
//         ease: "power1.inOut",
//         repeat: -1,
//         yoyo: true
//     });
// }

// let container = document.getElementById("panelContainer");

// gsap.to(container, {
//     x: () => -(container.scrollWidth - document.documentElement.clientWidth) + "px",
//     ease: "none",
//     scrollTrigger: {
//         trigger: container,
//         invalidateOnRefresh: true,
//         pin: true,
//         scrub: 1,
//         end: () => "+=" + container.offsetWidth
//     }
// })